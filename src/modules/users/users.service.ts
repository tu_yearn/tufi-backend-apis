import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('Usuario no existe');
    return user;
  }

  async create(dto: CreateUserDto): Promise<User> {
    const userExist = await this.userRepository.findOne({ email : dto.email });

    if (userExist) throw new BadRequestException('Correo ya existe');

    const newUser = this.userRepository.create(dto);
    const user = await this.userRepository.save(newUser);
    delete user.password;
    return user;
  }

  async update(id: number, dto: UpdateUserDto): Promise<User> {
    const userEdit = await this.findOne(id);

    const editedUser = Object.assign(userEdit, dto);
    const user = await this.userRepository.save(editedUser);
    delete user.password;
    return user;
  }

  async remove(id: number) {
    const user = await this.findOne(id);
    return await this.userRepository.remove(user);
  }

  async emailExist(user: CreateUserDto) {
    return await this.userRepository.findOne({ email : user.email });
  }
}
