import { ApiProperty } from '@nestjs/swagger';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { hash } from 'bcryptjs';

@Entity('users')
export class User {
  @ApiProperty()

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'first_name', type: 'varchar', length: 255, nullable:true })
  firstName: string;

  @Column({ name: 'last_name', type: 'varchar', length: 255, nullable:true })
  lastName: string;

  @Column({ type: 'varchar', length: 255, unique:true })
  email: string;

  @Column({ type: 'varchar', length: 128, select: false })
  password: string;

  @Column({ type: 'boolean', default: false })
  status: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (!this.password) {
      return;
    }
    this.password = await hash(this.password, 10);
  }
}
