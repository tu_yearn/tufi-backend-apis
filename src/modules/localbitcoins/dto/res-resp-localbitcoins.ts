export interface ResReqLocalbitcoins {
  data: ResReqBuyData;
 // pagination: Pagination;
}

interface ResReqBuyData {
  ad_list: ResAdList[];
  ad_count: number;
}

export interface ResAdList {
  data: AdListData;
  actions: Actions;
}

interface Actions {
  public_view: string;
}

export interface AdListData {
  profile: Profile;
  visible: boolean;
  hidden_by_opening_hours: boolean;
  location_string: string;
  countrycode: string;
  city: string;
  trade_type: string;
  online_provider: string;
  first_time_limit_btc: null | string;
  volume_coefficient_btc: string;
  sms_verification_required: boolean;
  currency: string;
  lat: number;
  lon: number;
  min_amount: null | string;
  max_amount: null | string;
  max_amount_available: string;
  ad_id: number;
  temp_price_usd: string;
  temp_price: number;
  created_at: Date;
  require_feedback_score: number;
  require_trade_volume: number;
  msg: string;
  bank_name: string;
  atm_model: null;
  require_trusted_by_advertiser: boolean;
  require_identification: boolean;
  is_local_office: boolean;
  payment_window_minutes: number;
  limit_to_fiat_amounts: null | string;
}

interface Profile {
  username: string;
  trade_count: string;
  feedback_score: number;
  name: string;
  last_online: Date;
}

/* interface Pagination {
  next: string | null;
  prev: string | null
} */
