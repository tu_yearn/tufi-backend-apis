import { HttpModule, Module } from '@nestjs/common';
import { LocalbitcoinsService } from './localbitcoins.service';
import { LocalbitcoinsController } from './localbitcoins.controller';

@Module({
  controllers: [LocalbitcoinsController],
  providers: [LocalbitcoinsService],
  imports: [HttpModule],
})
export class LocalbitcoinsModule {}
