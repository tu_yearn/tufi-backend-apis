import { HttpService, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';

import { urlLocalBitcoins } from '../../config/constants';
import {
  GetParamsCurrencyDto,
  GetParamsCountryDto,
  GetParamsPaymentDto,
  ResReqLocalbitcoins,
} from './dto';
import { AdListLocalbitcoins, Provider } from '../../models';

@Injectable()
export class LocalbitcoinsService {
  constructor(private httpService: HttpService) {}

  async getBitcoinsOnlineByCountry(
    data: GetParamsCountryDto,
    page: number = 1,
    price: number,
  ): Promise<{ res: AdListLocalbitcoins[]; next: boolean }> {
    let next: boolean;
    const res = await this.httpService
      .get(
        `${urlLocalBitcoins}/${data.trade}-bitcoins-online/${data.code}/${data.country}/.json?page=${page}`,
      )
      .pipe(
        map(response => {
          const query: ResReqLocalbitcoins = response.data;
          query.data.ad_count === 50 ? (next = true) : (next = false);
          return query.data.ad_list
            .filter(
              element =>
                element.data.temp_price > price * 0.8 &&
                element.data.temp_price < price * 1.2,
            )
            .map(ad => {
              ad.data.trade_type = data.trade;
              return AdListLocalbitcoins.adListJson(ad, Provider.localbitcoins);
            });
        }),
      )
      .toPromise();
    return { res, next };
  }

  async getBitcoinsOnlineByPaymentMethods(
    data: GetParamsPaymentDto,
    page: number = 1,
    price: number,
  ): Promise<{ res: AdListLocalbitcoins[]; next: boolean }> {
    let next: boolean;
    const res = await this.httpService
      .get(
        `${urlLocalBitcoins}/${data.trade}-bitcoins-online/${data.code}/${data.country}/${data.paymentMethods}/.json?page=${page}`,
      )
      .pipe(
        map(response => {
          const query: ResReqLocalbitcoins = response.data;
          query.data.ad_count === 50 ? (next = true) : (next = false);
          return query.data.ad_list
            .filter(
              element =>
                element.data.temp_price > price * 0.8 &&
                element.data.temp_price < price * 1.2,
            )
            .map(ad => {
              ad.data.trade_type = data.trade;
              return AdListLocalbitcoins.adListJson(ad, Provider.localbitcoins);
            });
        }),
      )
      .toPromise();
    return { res, next };
  }

  async getBitcoinsOnlineByCurrency(
    data: GetParamsCurrencyDto,
    page: number = 1,
    price: number,
  ): Promise<{ res: AdListLocalbitcoins[]; next: boolean }> {
    let next: boolean;
    const res = await this.httpService
      .get(
        `${urlLocalBitcoins}/${data.trade}-bitcoins-online/${data.currency}/${data.paymentMethods}/.json?page=${page}`,
      )
      .pipe(
        map(response => {
          const query: ResReqLocalbitcoins = response.data;
          query.data.ad_count === 50 ? (next = true) : (next = false);
          return query.data.ad_list
            .filter(
              element =>
                element.data.temp_price > price * 0.8 &&
                element.data.temp_price < price * 1.2,
            )
            .map(ad => {
              ad.data.trade_type = data.trade;
              return AdListLocalbitcoins.adListJson(ad, Provider.localbitcoins);
            });
        }),
      )
      .toPromise();
    return { res, next };
  }

  async getPriceMarket(currency: string): Promise<number> {
    const res = await this.httpService
      .get(
        `${urlLocalBitcoins}/api/equation/max(bitstampusd_avg, bitfinexusd_avg)*USD_in_${currency}`,
      )
      .toPromise();
    if (res.data === '') {
      return 0;
    }
    const price: number = res.data.data;
    return price;
  }
}
