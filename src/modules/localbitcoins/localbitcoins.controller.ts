import { Controller, Get, Param, Logger } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { currencyData } from 'src/config/data';

import {
  GetParamsCountryDto,
  GetParamsCurrencyDto,
  GetParamsPaymentDto,
} from './dto';
import { Ad, AdListLocalbitcoins } from '../../models';
import { LocalbitcoinsService } from './localbitcoins.service';
const logger = new Logger('LOCALBITCOINS');

@ApiTags('LOCALBITCOINS')
@Controller('localbitcoins')
export class LocalbitcoinsController {
  constructor(private readonly localbitcoinsService: LocalbitcoinsService) {}

  @Get('country/:trade/:code/:country')
  async getBitcoinsOnlineByCountry(
    @Param('trade') trade: string,
    @Param('code') code: string,
    @Param('country') country: string,
  ): Promise<Ad[]> {
    let dto: GetParamsCountryDto = { trade, code, country };
    let result: Ad[] = [];
    let i = 1;
    const cd = currencyData.find(cur => cur.code === code);
    const priceMarket = await this.localbitcoinsService.getPriceMarket(
      cd.currency,
    );
    do {
      const data: {
        res: AdListLocalbitcoins[];
        next: boolean;
      } = await this.localbitcoinsService.getBitcoinsOnlineByCountry(
        dto,
        i,
        priceMarket,
      );
      result = result.concat(data.res);
      logger.log(`Processing ads of ${dto.trade}, page Nro: ${i}`);
      i++;
      if (!data.next || i > 20) i = 0;
    } while (i > 0);

    logger.log(`Successful process ${dto.trade}`);
    return result;
  }

  @Get('country/:trade/:code/:country/:paymentMethods')
  async getBitcoinsOnlineByPaymentMethods(
    @Param('trade') trade: string,
    @Param('code') code: string,
    @Param('country') country: string,
    @Param('paymentMethods') paymentMethods: string,
  ): Promise<Ad[]> {
    let dto: GetParamsPaymentDto = { trade, code, country, paymentMethods };
    let result: Ad[] = [];
    const cd = currencyData.find(cur => cur.code === code);
    const priceMarket = await this.localbitcoinsService.getPriceMarket(
      cd.currency,
    );
    let i = 1;
    do {
      const data: {
        res: AdListLocalbitcoins[];
        next: boolean;
      } = await this.localbitcoinsService.getBitcoinsOnlineByPaymentMethods(
        dto,
        i,
        priceMarket,
      );
      result = result.concat(data.res);
      logger.log(`Processing ads of ${dto.trade}, page Nro: ${i}`);
      i++;
      if (!data.next || i > 20) i = 0;
    } while (i > 0);

    logger.log(`Successful process`);
    return result;
  }

  @Get('currency/:trade/:currency/:paymentMethods')
  async getBitcoinsOnlineByCurrency(
    @Param('trade') trade: string,
    @Param('currency') currency: string,
    @Param('paymentMethods') paymentMethods: string,
  ): Promise<Ad[]> {
    let dto: GetParamsCurrencyDto = { trade, currency, paymentMethods };
    const priceMarket = await this.localbitcoinsService.getPriceMarket(
      currency,
    );
    let result: Ad[] = [];
    let i = 1;
    do {
      const rr: {
        res: AdListLocalbitcoins[];
        next: boolean;
      } = await this.localbitcoinsService.getBitcoinsOnlineByCurrency(
        dto,
        i,
        priceMarket,
      );
      result = result.concat(rr.res);
      logger.log(`Processing ads of ${dto.trade}, page Nro: ${i}`);
      i++;
      if (!rr.next || i > 20) i = 0;
    } while (i > 0);

    logger.log(`Successful process`);
    return result;
  }
}
