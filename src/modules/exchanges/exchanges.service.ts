import { Injectable, Logger, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { getRepository, Repository } from "typeorm";
import {
  PaginationDto,
  PaginatedExchangeResultDto,
  UpdateExchangeDto,
  CreateExchangeDto,
} from "./dto";

import { Exchange } from "./entities/exchange.entity";
const logger = new Logger("ExchangeServices");

@Injectable()
export class ExchangesService {
  constructor(
    @InjectRepository(Exchange)
    private readonly exchangeRepository: Repository<Exchange>
  ) {}

  async findAll(
    paginationDto: PaginationDto
  ): Promise<PaginatedExchangeResultDto> {
    try {
      const distintCoinRepository = getRepository(Exchange);
      const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
      const direction =
        paginationDto.sortOrderDirection === "DESC" ? "DESC" : "ASC";
      const name = paginationDto.sortOrderName;
      const parameters = paginationDto.filter;

      const result = distintCoinRepository.createQueryBuilder();
      result.where(parameters);
      result.orderBy(name, direction);
      result.offset(skippedItems);
      result.limit(paginationDto.limit);
      const coins = await result.getMany();
      const count = await result.getCount();

      return {
        totalCount: count,
        page: paginationDto.page,
        limit: paginationDto.limit,
        data: coins,
      };
    } catch (error) {
      logger.error(error);
    }
  }

  async findOne(id: number): Promise<Exchange> {
    const exchange = await this.exchangeRepository.findOne(id);
    if (!exchange) throw new NotFoundException("Exchange no existe");
    return exchange;
  }

  async create(dto: CreateExchangeDto): Promise<Exchange> {
    const newExchange = this.exchangeRepository.create(dto);
    const exchange = await this.exchangeRepository.save(newExchange);
    return exchange;
  }

  async update(id: number, dto: UpdateExchangeDto): Promise<Exchange> {
    const userEdit = await this.findOne(id);

    const editedExchange = Object.assign(userEdit, dto);
    const exchange = await this.exchangeRepository.save(editedExchange);
    return exchange;
  }

  async remove(id: number) {
    const exchange = await this.findOne(id);
    return await this.exchangeRepository.remove(exchange);
  }
}
