import { Exchange } from './../entities/exchange.entity';

export class PaginatedExchangeResultDto {
  data: Exchange[];
  page: number;
  limit: number;
  totalCount: number;
}
