import { IsOptional, IsNumber, IsString } from 'class-validator';

export class PaginationDto {
  @IsOptional()
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  limit: number;

  @IsOptional()
  @IsString()
  sortOrderName: string;

  @IsOptional()
  @IsString()
  sortOrderDirection: string;

  @IsOptional()
  @IsString()
  filter: string;
}
