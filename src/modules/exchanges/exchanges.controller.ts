import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseIntPipe,
  Query,
} from "@nestjs/common";
import { ExchangesService } from "./exchanges.service";
import {
  UpdateExchangeDto,
  CreateExchangeDto,
  PaginationDto,
  PaginatedExchangeResultDto,
} from "./dto";
import { ApiTags } from "@nestjs/swagger";

@ApiTags("EXCHANGES")
@Controller("exchanges")
export class ExchangesController {
  constructor(private readonly exchangesService: ExchangesService) {}

  @Get()
  async findAll(
    @Query("page") page: number = 1,
    @Query("limit") limit: number = 100,
    @Query("sortOrderName") sortOrderName: string = "market_share_by_volume",
    @Query("sortOrderDirection") sortOrderDirection: string = "DESC",
    @Query("filter") filter: string
  ): Promise<PaginatedExchangeResultDto> {
    let paginationDto: PaginationDto = {
      page,
      limit,
      sortOrderName,
      sortOrderDirection,
      filter,
    };
    console.log(filter);
    return await this.exchangesService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 250 ? 250 : paginationDto.limit,
    });
  }

  @Get(":id")
  async findOne(@Param("id", ParseIntPipe) id: number) {
    const data = await this.exchangesService.findOne(id);
    return {
      message: "Consulta existosa",
      data,
    };
  }

  @Post()
  async create(@Body() createExchangeDto: CreateExchangeDto) {
    const data = await this.exchangesService.create(createExchangeDto);
    return {
      message: "Exchange created",
      data,
    };
  }

  @Put(":id")
  async update(
    @Param("id", ParseIntPipe) id: number,
    @Body() updateExchangeDto: UpdateExchangeDto
  ) {
    const data = await this.exchangesService.update(+id, updateExchangeDto);
    return {
      message: "Exchange edited",
      data,
    };
  }

  @Delete(":id")
  async remove(@Param("id", ParseIntPipe) id: number) {
    const data = await this.exchangesService.remove(id);
    return {
      message: "Exchange deleted",
      data,
    };
  }
}
