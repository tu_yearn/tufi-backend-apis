import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm';

@Entity('payment_methods')
export class PaymentMethod {
  @ApiProperty()

  @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'code', type: 'varchar', length: 255 })
  code: string;

  @Column({ name: 'description', type: 'varchar', length: 255 })
  description: string;

}
