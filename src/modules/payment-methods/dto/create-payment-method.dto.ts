import { IsOptional, IsString, MaxLength } from "class-validator";

export class CreatePaymentMethodDto {

  @IsOptional()
  @IsString()
  @MaxLength(255)
  code: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  description: string;

}
