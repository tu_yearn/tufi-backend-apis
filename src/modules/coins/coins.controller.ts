import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseIntPipe,
  Query,
  Logger,
} from '@nestjs/common';
import { ListCoinsCoingeckoMarkets } from 'src/models';
import { CoinsService } from './coins.service';
import {
  PaginationDto,
  PaginatedCoinsResultDto,
  UpdateCoinDto,
} from 'src/modules/coins/dto';
import { ApiTags } from '@nestjs/swagger';

const logger = new Logger('Coins Controlers');

@ApiTags('COINS')
@Controller('coins')
export class CoinsController {
  constructor(private readonly coinsService: CoinsService) {}

  @Get()
  async findAll(
    @Query('page') page: number = 1,
    @Query('limit') limit: number = 100,
    @Query('sortOrderName') sortOrderName: string = 'market_cap',
    @Query('sortOrderDirection') sortOrderDirection: string = 'DESC',
    @Query('filter') filter: string,
  ): Promise<PaginatedCoinsResultDto> {
    let paginationDto: PaginationDto = {
      page,
      limit,
      sortOrderName,
      sortOrderDirection,
      filter
    };
    console.log(filter);
    return this.coinsService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 250 ? 250 : paginationDto.limit,
    });
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    const data = await this.coinsService.findOne(id);
    return {
      message: 'Consulta existosa',
      data,
    };
  }

  @Post()
  async create() {
    logger.log('Actualizando tabla coins');
    setInterval(async () => {
        let dto: ListCoinsCoingeckoMarkets[] = [];
        for (let index = 1; index < 26; index++) {
            dto = dto.concat(await this.coinsService.getCoingecko(index));
        }
        await this.coinsService.create(dto);
        return {
            message: 'User created',
        };
    }, 300000);
    return []
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCoinDto: UpdateCoinDto) {
    return this.coinsService.update(+id, updateCoinDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.coinsService.remove(+id);
  }
}
