import {
  Column,
  Connection,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ViewColumn,
  ViewEntity,
} from "typeorm";

@Entity("coins")
export class Coin {
  @PrimaryGeneratedColumn()
  pk: number;
  @Column({ type: "varchar" })
  id: string;

  @Column({ type: "varchar" })
  symbol: string;

  @Column({ type: "varchar" })
  name: string;

  @Column({ type: "varchar", nullable: true })
  image: string;

  @Column({ type: "float", nullable: true })
  current_price: number;

  @Column({ type: "float", nullable: true })
  market_cap: number;

  @Column({ type: "int", nullable: true })
  market_cap_rank: number;

  @Column({ type: "float", nullable: true })
  fully_diluted_valuation: number;

  @Column({ type: "float", nullable: true })
  total_volume: number;

  @Column({ type: "float", nullable: true })
  high_24h: number;

  @Column({ type: "float", nullable: true })
  low_24h: number;

  @Column({ type: "float", nullable: true })
  price_change_24h: number;

  @Column({ type: "float", nullable: true })
  price_change_percentage_24h: number;

  @Column({ type: "float", nullable: true })
  market_cap_change_24h: number;

  @Column({ type: "float", nullable: true })
  market_cap_change_percentage_24h: number;

  @Column({ type: "float", nullable: true })
  circulating_supply: number;

  @Column({ type: "float", nullable: true })
  total_supply: number;

  @Column({ type: "float", nullable: true })
  max_supply: number;

  @Column({ type: "float", nullable: true })
  ath: number;

  @Column({ type: "float", nullable: true })
  ath_change_percentage: number;

  @Column({ type: "date", nullable: true })
  ath_date: Date;

  @Column({ type: "float", nullable: true })
  atl: number;

  @Column({ type: "float", nullable: true })
  atl_change_percentage: number;

  @Column({ type: "date", nullable: true })
  atl_date: Date;

  @Column({ type: "date", nullable: true })
  last_updated: Date;

  @Column({ type: "simple-array", nullable: true })
  sparkline_in_7d: number[];

  @Column({ type: "float", nullable: true })
  price_change_percentage_1h_in_currency: number;

  @Column({ type: "float", nullable: true })
  price_change_percentage_24h_in_currency: number;

  @Column({ type: "float", nullable: true })
  price_change_percentage_7d_in_currency: number;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;
}

@ViewEntity({
  expression: (connection: Connection) =>
    connection
      .createQueryBuilder(Coin, 'coins')
      .select([
        'id',
        'symbol',
        'name',
        'image',
        'current_price',
        'market_cap',
        'market_cap_rank',
        'fully_diluted_valuation',
        'total_volume',
        'high_24h',
        'low_24h',
        'price_change_24h',
        'price_change_percentage_24h',
        'market_cap_change_24h',
        'market_cap_change_percentage_24h',
        'circulating_supply',
        'total_supply',
        'max_supply',
        'ath',
        'ath_change_percentage',
        'ath_date',
        'atl',
        'atl_change_percentage',
        'atl_date',
        'last_updated',
        'sparkline_in_7d',
        'price_change_percentage_1h_in_currency',
        'price_change_percentage_24h_in_currency',
        'price_change_percentage_7d_in_currency',
      ])
      .distinct(true),
})
export class DistintCoin {
  @ViewColumn()
    id: string;

  @ViewColumn()
  symbol: string;

  @ViewColumn()
  name: string;

  @ViewColumn()
  image: string;

  @ViewColumn()
  current_price: number;

  @ViewColumn()
  market_cap: number;

  @ViewColumn()
  market_cap_rank: number;

  @ViewColumn()
  fully_diluted_valuation: number;

  @ViewColumn()
  total_volume: number;

  @ViewColumn()
  high_24h: number;

  @ViewColumn()
  low_24h: number;

  @ViewColumn()
  price_change_24h: number;

  @ViewColumn()
  price_change_percentage_24h: number;

  @ViewColumn()
  market_cap_change_24h: number;

  @ViewColumn()
  market_cap_change_percentage_24h: number;

  @ViewColumn()
  circulating_supply: number;

  @ViewColumn()
  total_supply: number;

  @ViewColumn()
  max_supply: number;

  @ViewColumn()
  ath: number;

  @ViewColumn()
  ath_change_percentage: number;

  @ViewColumn()
  ath_date: Date;

  @ViewColumn()
  atl: number;

  @ViewColumn()
  atl_change_percentage: number;

  @ViewColumn()
  atl_date: Date;

  @ViewColumn()
  last_updated: Date;

  @ViewColumn()
  sparkline_in_7d: number[];

  @ViewColumn()
  price_change_percentage_1h_in_currency: number;

  @ViewColumn()
  price_change_percentage_24h_in_currency: number;

  @ViewColumn()
  price_change_percentage_7d_in_currency: number;
}
