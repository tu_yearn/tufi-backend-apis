import { map } from 'rxjs/operators';
import {
  Injectable,
  HttpService,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';

import { Coin, DistintCoin } from './entities/coin.entity';
import {
  UpdateCoinDto,
  ReqCoinDto,
  PaginationDto,
  PaginatedCoinsResultDto,
} from './dto';
import { ListCoinsCoingeckoMarkets } from 'src/models';

const logger = new Logger('CoinsServices');
@Injectable()
export class CoinsService {
  constructor(
    @InjectRepository(Coin)
    private readonly coinRepository: Repository<Coin>,
    private httpService: HttpService,
  ) {}

  async create(dto: ListCoinsCoingeckoMarkets[]): Promise<Coin[]> {
    try {
      this.coinRepository.clear();
      const newCoin = this.coinRepository.create(dto);
      const coin = await this.coinRepository.save(newCoin);
      logger.log('Datos actualizados');
      return coin;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async findAll(
    paginationDto: PaginationDto,
  ): Promise<PaginatedCoinsResultDto> {
    try {
      const distintCoinRepository = getRepository(DistintCoin);
      const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
      const direction =
        paginationDto.sortOrderDirection === 'DESC' ? 'DESC' : 'ASC';
      const name = paginationDto.sortOrderName;
      const parameters = paginationDto.filter;

      const result = distintCoinRepository.createQueryBuilder();
      result.where(parameters);
      result.orderBy(name, direction);
      result.offset(skippedItems);
      result.limit(paginationDto.limit);
      const coins = await result.getMany();
      const count = await result.getCount();

      return {
        totalCount: count,
        page: paginationDto.page,
        limit: paginationDto.limit,
        data: coins,
      };
    } catch (error) {
      logger.error(error);
    }
  }

  async findOne(id: number): Promise<Coin> {
    const coin = await this.coinRepository.findOne(id);
    if (!coin) throw new NotFoundException('Coins no existe');
    return coin;
  }

  update(id: number, updateCoinDto: UpdateCoinDto) {
    return `This action updates a #${id} coin`;
  }

  remove(id: number) {
    return `This action removes a #${id} coin`;
  }

  async getCoingecko(page: number): Promise<ListCoinsCoingeckoMarkets[]> {
    try {
      logger.log(`Coingecko - Page: ${page}`);
      const res = await this.httpService
        .get(
          `https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=250&page=${page}&sparkline=true&price_change_percentage=1h,24h,7d`,
        )
        .pipe(
          map(response => {
            const query: ListCoinsCoingeckoMarkets[] = response.data.map(
              (ad: ReqCoinDto) => {
                ad.image = ad.image.replace('large', 'thumb');
                return ListCoinsCoingeckoMarkets.adListJson(ad);
              },
            );
            return query;
          }),
        )
        .toPromise();
      return res;
    } catch (error) {
      console.log(error.response);
      return [];
    }
  }
}
