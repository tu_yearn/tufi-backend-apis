import { LocalbitcoinsService } from './../localbitcoins/localbitcoins.service';
import { Controller, Get, Param, Logger } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  GetParamsCountryDto,
  GetParamsCurrencyDto,
  GetParamsPaymentDto,
} from './dto';
import { Ad } from '../../models';
import { LocalCoinsSwapService } from './local-coins-swap.service';
import { currencyData } from 'src/config/data';
const logger = new Logger('LOCALCOINSWAP');

@ApiTags('LOCALCOINSWAP')
@Controller('localcoinswap')
export class LocalCoinsSwapController {
  constructor(
    private readonly localCoinsSwapService: LocalCoinsSwapService,
    private readonly localbitcoinsService: LocalbitcoinsService,
  ) {}

  @Get('country/:trade/:code/:country')
  async getBitcoinsOnlineByCountry(
    @Param('trade') trade: string,
    @Param('code') code: string,
    @Param('country') country: string,
  ): Promise<Ad[]> {
    let dto: GetParamsCountryDto = { trade, code, country };
    const cd = currencyData.find(cur => cur.code === code);
    const priceMarket = await this.localbitcoinsService.getPriceMarket(
      cd.currency,
    );
    logger.log(`Successful process`);
    return await this.localCoinsSwapService.getBitcoinsOnlineByCountry(
      dto,
      priceMarket,
      cd.currency,
    );
  }

  @Get('country/:trade/:code/:country/:paymentMethods')
  async getBitcoinsOnlineByPaymentMethods(
    @Param('trade') trade: string,
    @Param('code') code: string,
    @Param('country') country: string,
    @Param('paymentMethods') paymentMethods: string,
  ): Promise<Ad[]> {
    let dto: GetParamsPaymentDto = { trade, code, country, paymentMethods };
    const cd = currencyData.find(cur => cur.code === code);
    const priceMarket = await this.localbitcoinsService.getPriceMarket(
      cd.currency,
    );
    let result: Ad[] = [];
    result = await this.localCoinsSwapService.getBitcoinsOnlineByPaymentMethods(
      dto,
      priceMarket,
      cd.currency,
    );
    logger.log(`Successful process`);
    return result;
  }

  @Get('currency/:trade/:currency/:paymentMethods')
  async getBitcoinsOnlineByCurrency(
    @Param('trade') trade: string,
    @Param('currency') currency: string,
    @Param('paymentMethods') paymentMethods: string,
  ): Promise<Ad[]> {
    let dto: GetParamsCurrencyDto = { trade, currency, paymentMethods };
    const priceMarket = await this.localbitcoinsService.getPriceMarket(
      currency,
    );
    let result: Ad[] = [];

    result = await this.localCoinsSwapService.getBitcoinsOnlineByCurrency(
      dto,
      priceMarket,
    );

    logger.log(`Successful process`);
    return result;
  }
}
