import { LocalbitcoinsModule } from './../localbitcoins/localbitcoins.module';
import { LocalbitcoinsService } from './../localbitcoins/localbitcoins.service';
import { HttpModule, Module } from '@nestjs/common';
import { LocalCoinsSwapController } from './localcoinswap.controller';
import { LocalCoinsSwapService } from './local-coins-swap.service';

@Module({
  controllers: [LocalCoinsSwapController],
  providers: [LocalCoinsSwapService, LocalbitcoinsService],
  imports: [HttpModule, LocalbitcoinsModule],
})
export class LocalCoinsSwapModule {}
