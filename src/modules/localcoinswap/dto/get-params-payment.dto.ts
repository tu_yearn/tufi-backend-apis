export class GetParamsPaymentDto {
    trade:string;
    code: string;
    country: string;
    paymentMethods: string;
}
