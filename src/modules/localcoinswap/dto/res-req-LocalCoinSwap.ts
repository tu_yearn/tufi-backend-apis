export interface ResRepLocalCoinSwap {
    active_page: number;
    count:       number;
    next:        string;
    previous:    null;
    offset:      number;
    limit:       number;
    total_pages: number;
    results:     adLocalCoinSwap[];
}

export interface adLocalCoinSwap {
    uuid:                      string;
    created_by:                CreatedBy;
    trading_type:              TradingType;
    coin_currency:             Currency;
    fiat_currency:             Currency;
    payment_method:            PaymentMethod;
    location_name:             string;
    country_code:              string;
    min_trade_size:            string;
    max_trade_size:            string;
    headline:                  string;
    photo_id_required:         boolean;
    sms_required:              boolean;
    minimum_feedback:          string;
    price_formula_value:       number;
    current_margin_percentage: string;
    slug:                      string;
    automatic_cancel_time:     string;
    enforced_sizes:            string;
    popularity:                string;
}

interface Currency {
    title:           string;
    symbol:          string;
    symbol_filename: null | string;
    is_crypto:       boolean;
}

interface CreatedBy {
    username:          string;
    activity_status:   string;
    activity_tooltip:  string;
    is_email_verified: boolean;
    is_phone_verified: boolean;
    avg_response_time: number;
    last_seen:         number;
    is_legacy:         boolean;
    user_uuid:         string;
    completed_trades:  number;
    ratings:           number | null;
}

interface PaymentMethod {
    name: string;
    slug: string;
}

interface TradingType {
    slug:          string;
    name:          string;
    opposite_name: string;
}
