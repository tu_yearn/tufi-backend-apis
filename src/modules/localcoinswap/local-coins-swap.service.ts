import { HttpService, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';

import {
  GetParamsCurrencyDto,
  GetParamsCountryDto,
  GetParamsPaymentDto,
  ResRepLocalCoinSwap,
} from './dto';
import { AdListLocalCoinsSwap, Provider } from '../../models';

@Injectable()
export class LocalCoinsSwapService {
  constructor(private httpService: HttpService) {}

  async getBitcoinsOnlineByCountry(
    data: GetParamsCountryDto,
    price: number,
    currency: string,
  ): Promise<AdListLocalCoinsSwap[]> {
    const res = await this.httpService
      .get(
        `https://localcoinswap.com/api/v2/offers/search/?coin_currency=BTC&limit=1100&fiat_currency=${currency}&payment_method=bank-transfers`,
      )
      .pipe(
        map(response => {
          let query: ResRepLocalCoinSwap = response.data;
          return query.results
            .filter(
              element =>
                element.country_code === data.code &&
                element.price_formula_value > price * 0.8 &&
                element.price_formula_value < price * 1.2,
            )
            .map(ad =>
              AdListLocalCoinsSwap.adListJson(ad, Provider.localcoinswap),
            );
        }),
      )
      .toPromise();
    return res;
  }

  async getBitcoinsOnlineByPaymentMethods(
    data: GetParamsPaymentDto,
    price: number,
    currency: string,
  ): Promise<AdListLocalCoinsSwap[]> {
    const res = await this.httpService
      .get(
        `https://localcoinswap.com/api/v2/offers/search/?coin_currency=BTC&limit=1200&fiat_currency=${currency}&payment_method=${data.paymentMethods}`,
      )
      .pipe(
        map(response => {
          let query: ResRepLocalCoinSwap = response.data;
          return query.results
            .filter(
              element =>
                element.country_code === data.code &&
                element.price_formula_value > price * 0.8 &&
                element.price_formula_value < price * 1.2,
            )
            .map(ad =>
              AdListLocalCoinsSwap.adListJson(ad, Provider.localcoinswap),
            );
        }),
      )
      .toPromise();

    return res;
  }

  async getBitcoinsOnlineByCurrency(
    data: GetParamsCurrencyDto,
    price: number,
  ): Promise<AdListLocalCoinsSwap[]> {
    let res = await this.httpService
      .get(
        `https://localcoinswap.com/api/v2/offers/search/?coin_currency=BTC&limit=1200&fiat_currency=${data.currency}&payment_method=${data.paymentMethods}`,
      )
      .pipe(
        map(response => {
          let query: ResRepLocalCoinSwap = response.data;
          return query.results
            .filter(
              element =>
                element.price_formula_value > price * 0.8 &&
                element.price_formula_value < price * 1.2,
            )
            .map(ad =>
              AdListLocalCoinsSwap.adListJson(ad, Provider.localcoinswap),
            );
        }),
      )
      .toPromise();
    if (res.length === 0) {
      res = await this.getBitcoinsDefault(data, price);
    }
    return res;
  }

  async getBitcoinsDefault(
    data: GetParamsCurrencyDto,
    price: number,
  ): Promise<AdListLocalCoinsSwap[]> {
    const res = await this.httpService
      .get(
        `https://localcoinswap.com/api/v2/offers/search/?coin_currency=BTC&limit=1200&fiat_currency=${data.currency}&payment_method=bank-transfers`,
      )
      .pipe(
        map(response => {
          let query: ResRepLocalCoinSwap = response.data;
          return query.results
            .filter(
              element =>
                element.price_formula_value > price * 0.8 &&
                element.price_formula_value < price * 1.2,
            )
            .map(ad =>
              AdListLocalCoinsSwap.adListJson(ad, Provider.localcoinswap),
            );
        }),
      )
      .toPromise();
    return res;
  }
}
