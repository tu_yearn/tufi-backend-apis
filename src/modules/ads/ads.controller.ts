import { Ad } from './../../models/ad.interface';
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Logger,
  Query,
} from '@nestjs/common';
import { AdListLocalbitcoins } from 'src/models';
import { AdsService } from './ads.service';
import { UpdateAdDto } from './dto/update-ad.dto';
import { PaginatedAdsResultDto, PaginationDto } from './dto';

const logger = new Logger('AdController');

@Controller('ads')
export class AdsController {
  constructor(private readonly adsService: AdsService) {}

  @Post()
  async create(@Query('currency') currency: string = 'VES') {
    logger.log('Actualizando tabla Ads');
    setInterval(async () => {
        let result: Ad[];
        const trade = ['buy', 'sell'];
        trade.forEach(async element => {
        let i = 1;
        result = [];
        do {
            const data: {
            res: AdListLocalbitcoins[];
            next: boolean;
            } = await this.adsService.getLocalbitcoins(currency, element, i);
            result = result.concat(data.res);
            logger.log(`Processing ads of ${element}, page Nro: ${i}`);
            i++;
            if (!data.next) i = 0;
        } while (i > 0);
        await this.adsService.create('localbitcoins',currency,result);
        logger.log(`Successful process ${element}`);
        });
    }, 300000);
    return {
      message: 'Table Ads created',
      data: [],
    };
  }

  @Get()
  async findAll(
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('sortOrderName') sortOrderName: string,
    @Query('sortOrderDirection') sortOrderDirection: string,
    @Query('filter') filter: string,
    @Query('currency') currency: string,
    @Query('countrycode') countrycode: string,
    @Query('online_provider') online_provider: string,
    @Query('textSearch') textSearch: string,

  ): Promise<PaginatedAdsResultDto> {
    let paginationDto: PaginationDto = {
      page,
      limit,
      sortOrderName,
      sortOrderDirection,
      filter,
      currency,
      countrycode,
      online_provider,
      textSearch
    };
    console.log(paginationDto);
    return this.adsService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 250 ? 250 : paginationDto.limit,
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.adsService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateAdDto: UpdateAdDto) {
    return this.adsService.update(+id, updateAdDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.adsService.remove(+id);
  }
}
