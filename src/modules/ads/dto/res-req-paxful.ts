export interface ResReqPaxful {
    status:    string;
    timestamp: number;
    data:      Data;
}

interface Data {
    count:      number;
    totalCount: number;
    offers:     AdPaxful[];
}

export interface AdPaxful {
    offer_id:                      string;
    offer_link:                    string;
    offer_type:                    string;
    payment_window:                number;
    currency_code:                 string;
    fiat_price_per_btc:            number;
    fiat_price_per_crypto:         number;
    fiat_USD_price_per_btc:        number;
    fiat_usd_price_per_crypto:     number;
    fiat_amount_range_min:         number;
    fiat_amount_range_max:         number;
    crypto_min:                    number;
    crypto_max:                    number;
    payment_method_name:           PaymentMethodName;
    active:                        boolean;
    payment_method_slug:           PaymentMethodSlug;
    payment_method_group:          PaymentMethodGroup;
    offer_owner_username:          string;
    offer_owner_profile_link:      string;
    offer_owner_feedback_positive: number;
    offer_owner_feedback_negative: number;
    offer_owner_country_iso:       null;
    last_seen:                     LastSeen;
    last_seen_timestamp:           number;
    require_verified_email:        boolean;
    require_verified_phone:        boolean;
    require_min_past_trades:       null;
    require_verified_id:           boolean;
    country_name:                  null;
    city_name:                     null;
    subdivision_name:              null;
    geoname_id:                    null;
    seller_fee:                    number;
    margin:                        number;
    payment_method_label:          string;
    offer_terms:                   string;
    is_blocked:                    boolean;
    tags:                          Tag[];
    duty_hours:                    DutyHour[];
}

enum CurrencyCode {
    Usd = "USD",
}

interface DutyHour {
    day:        number;
    active:     number;
    start_time: Time;
    end_time:   Time;
}

enum Time {
    The0000 = "00:00",
}

enum LastSeen {
    SeenRecently = "seen-recently",
    SeenVeryRecently = "seen-very-recently",
}

enum OfferType {
    Buy = "buy",
    Sell = "sell",
}

enum PaymentMethodGroup {
    BankTransfers = "bank-transfers",
}

enum PaymentMethodName {
    BankTransfer = "Bank Transfer",
}

enum PaymentMethodSlug {
    BankTransfer = "bank-transfer",
}

interface Tag {
    name:        string;
    slug:        string;
    description: string;
}
