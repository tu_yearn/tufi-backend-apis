import { IsOptional, IsNumber, IsString } from 'class-validator';

export class PaginationDto {
  @IsOptional()
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  limit: number;

  @IsOptional()
  @IsString()
  sortOrderName: string;

  @IsOptional()
  @IsString()
  sortOrderDirection: string;

  @IsOptional()
  @IsString()
  filter: string;

  @IsOptional()
  @IsString()
  currency: string;

  @IsOptional()
  @IsString()
  countrycode: string;

  @IsOptional()
  @IsString()
  online_provider: string;

  @IsOptional()
  @IsString()
  textSearch: string;
}
