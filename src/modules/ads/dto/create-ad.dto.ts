import { IsOptional, IsString, IsNumber } from "class-validator";

export class CreateAdDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  feedback_score: number;

  @IsOptional()
  @IsString()
  trade_count: string;

  @IsOptional()
  @IsString()
  last_online: Date;

  @IsOptional()
  visible: boolean;

  @IsOptional()
  @IsNumber()
  temp_price: number;

  @IsOptional()
  @IsString()
  online_provider: string;

  @IsOptional()
  @IsString()
  bank_name: string;

  @IsOptional()
  @IsNumber()
  min_amount: number;

  @IsOptional()
  @IsNumber()
  max_amount: number;

  @IsOptional()
  @IsString()
  location_string: string;

  @IsOptional()
  @IsNumber()
  payment_window_minutes: number;

  @IsOptional()
  @IsString()
  msg: string;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  @IsString()
  public_view: string;

  @IsOptional()
  @IsString()
  countrycode: string;

  @IsOptional()
  @IsString()
  trade_type: string;

  @IsOptional()
  @IsString()
  fiat_currency: string;

  @IsOptional()
  @IsString()
  coin_currency: string;

  @IsOptional()
  @IsString()
  provider: string;
}
