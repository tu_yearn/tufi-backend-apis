export interface ResReqHodl {
    status:     string;
    filters:    Filters;
    sort:       Filters;
    pagination: Filters;
    offers:     adHodl[];
}

export interface Filters {
}

export interface adHodl {
    id:                           string;
    version:                      string;
    asset_code:                   AssetCode;
    searchable:                   boolean;
    country:                      string;
    country_code:                 string;
    working_now:                  boolean;
    side:                         Side;
    title:                        null | string;
    description:                  null | string;
    currency_code:                string;
    price:                        string;
    min_amount:                   string;
    max_amount:                   string;
    first_trade_limit:            null | string;
    fee:                          Fee;
    balance:                      null | string;
    payment_window_minutes:       number;
    confirmations:                number;
    payment_method_instructions?: PaymentMethodInstruction[];
    trader:                       Trader;
    payment_methods?:             PaymentMethod[];
}

enum AssetCode {
    Btc = "BTC",
}

interface Fee {
    author_fee_rate: string;
}

interface PaymentMethodInstruction {
    id:                  string;
    version:             string;
    payment_method_id:   string;
    payment_method_type: Type;
    payment_method_name: string;
}

enum Type {
    ATMWithdrawal = "ATM withdrawal",
    BankWire = "Bank wire",
    Cash = "Cash",
    OnlinePaymentSystem = "Online payment system",
}

interface PaymentMethod {
    id:   string;
    type: Type;
    name: string;
}

enum Side {
    Buy = "buy",
    Sell = "sell",
}

interface Trader {
    login:                        string;
    avatar_url:                   string;
    online_status:                OnlineStatus;
    rating:                       null | string;
    trades_count:                 number;
    url:                          string;
    verified:                     boolean;
    verified_by:                  null | string;
    strong_hodler:                boolean;
    country:                      string;
    country_code:                 string;
    average_payment_time_minutes: number | null;
    average_release_time_minutes: number | null;
    days_since_last_trade:        number | null;
    blocked_by:                   number;
}

enum OnlineStatus {
    Offline = "offline",
    Online = "online",
    RecentlyOnline = "recently_online",
}
