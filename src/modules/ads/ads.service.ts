import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  Injectable,
  HttpService,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { map } from 'rxjs/operators';

import { urlLocalBitcoins } from 'src/config/constants';
import {
  AdListLocalbitcoins,
  AdListLocalCoinsSwap,
  Provider,
  AdListHodl,
  AdListPaxful
} from 'src/models';
import { ResReqLocalbitcoins } from '../localbitcoins/dto';
import { Ad } from './entities/ad.entity';
import { ResRepLocalCoinSwap } from '../localcoinswap/dto';
import {
    PaginatedAdsResultDto,
    PaginationDto,
    CreateAdDto,
    UpdateAdDto,
    ResReqHodl,
    ResReqPaxful
} from './dto';
import { currencyData } from 'src/config/data';

const logger = new Logger('AdServices');

@Injectable()
export class AdsService {
  constructor(
    @InjectRepository(Ad)
    private readonly adRepository: Repository<Ad>,
    private httpService: HttpService,
  ) {}

  async create(
    provider: string,
    cur: string,
    dto: CreateAdDto[]
  ) {
    const cd = currencyData.find(c => c.currency === cur);
    try {
        const fechaHora = new Date();
        const h = fechaHora.getHours();
        const m = fechaHora.getMinutes();
        logger.log('Hora: ' + h + ':' + m + ' ' + cd.code);
        if (h === 4 && m >= 54) {
          await this.adRepository.clear();
        } else {
          await this.adRepository
            .createQueryBuilder()
            .delete()
            .where('provider = :p', { p: provider })
            .andWhere('countrycode = :country', { country: cd.code })
            .execute();
        }
      const newAd = this.adRepository.create(dto);
      const ads = await this.adRepository.save(newAd);
      logger.log('Datos actualizados');
      return ads;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async createAll(provider: string, dto: CreateAdDto[]) {
    try {
        await this.adRepository
          .createQueryBuilder()
          .delete()
          .where('provider = :p', { p: provider })
          .andWhere('countrycode NOT IN (:...country)', { country: ['VE'] })
          .execute();
      const newAd = this.adRepository.create(dto);
      const ads = await this.adRepository.save(newAd);
      logger.log('Datos actualizados');
      return ads;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async createLocalCoinSwap(coin_currency:string, provider: string, dto: CreateAdDto[]) {
    try {
        await this.adRepository
          .createQueryBuilder()
          .delete()
          .where('provider = :p', { p: provider })
          .andWhere('coin_currency = :c', { c: coin_currency })
          .execute();
      const newAd = this.adRepository.create(dto);
      const ads = await this.adRepository.save(newAd);
      logger.log('Datos actualizados: '+ coin_currency);
      return ads;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async createHodl(provider: string, dto: CreateAdDto[]) {
    try {
        await this.adRepository
          .createQueryBuilder()
          .delete()
          .where('provider = :p', { p: provider })
          .execute();
      const newAd = this.adRepository.create(dto);
      const ads = await this.adRepository.save(newAd);
      logger.log('Datos actualizados...');
      return ads;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async createPaxful(trade: string, currency: string, provider: string, dto: CreateAdDto[]) {
    try {
        await this.adRepository
          .createQueryBuilder()
          .delete()
          .where('provider = :p AND trade_type = :t AND fiat_currency = :c', { p: provider, t: trade, c: currency })
          .execute();
      const newAd = this.adRepository.create(dto);
      const ads = await this.adRepository.save(newAd);
      logger.log(`Datos actualizados...Paxful - ${trade}`);
      return ads;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedAdsResultDto> {
    try {
      const priceMarket = await this.getPriceMarket(paginationDto.currency);
      logger.log('Precio market:' + priceMarket);
      const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
      const direction =
        paginationDto.sortOrderDirection === 'DESC' ? 'DESC' : 'ASC';
      const name = paginationDto.sortOrderName;
      const parameters = paginationDto.filter;
      const currency = paginationDto.currency;
      const countrycode = paginationDto.countrycode;
      const textSearch = paginationDto.textSearch;

      const online_provider = paginationDto.online_provider;
      const result = this.adRepository.createQueryBuilder();
      result.where(parameters);
      if (textSearch != '') {
        result.andWhere('bank_name LIKE :m OR name LIKE :m', {
          m: `%${textSearch}%`,
        });
      }
      if (currency != '') {
        result.andWhere('currency = :x', { x: currency });
      }
      if (countrycode != '') {
        result.andWhere('countrycode = :y', { y: countrycode });
      }
      if (online_provider != '') {
        result.andWhere('online_provider = :z', { z: online_provider });
      }
      result.andWhere('temp_price BETWEEN :a AND :b', {
        a: priceMarket * 0.7,
        b: priceMarket * 1.3,
      });
      result.orderBy(name, direction);
      result.offset(skippedItems);
      result.limit(paginationDto.limit);
      const ads = await result.getMany();
      const count = await result.getCount();
      return {
        totalCount: count,
        page: paginationDto.page,
        limit: paginationDto.limit,
        data: ads,
      };
    } catch (error) {
      logger.error(error);
    }
  }

  async findOne(id: number): Promise<Ad> {
    const ad = await this.adRepository.findOne(id);
    if (!ad) throw new NotFoundException('Ads no existe');
    return ad;
  }

  update(id: number, updateAdDto: UpdateAdDto) {
    return `This action updates a #${id} ad`;
  }

  remove(id: number) {
    return `This action removes a #${id} ad`;
  }

  async getLocalbitcoins(
    currency: string,
    trade: string,
    page: number,
  ): Promise<{ res: AdListLocalbitcoins[]; next: boolean }> {
    let next = false;
    const cd = currencyData.find(cur => cur.currency === currency);
    const res = await this.httpService
      .get(
        `${urlLocalBitcoins}/${trade}-bitcoins-online/${
          cd.code
        }/${cd.name.toLowerCase()}/.json?page=${page}`,
      )
      .pipe(
        map(response => {
          const query: ResReqLocalbitcoins = response.data;
          query.data.ad_count === 50 ? (next = true) : (next = false);
          return query.data.ad_list.map(ad => {
            ad.data.trade_type = trade;
            return AdListLocalbitcoins.adListJson(ad, Provider.localbitcoins);
          });
        }),
      )
      .toPromise();
    return { res, next };
  }

  async getLocalbitcoinsAll(
    trade: string,
    page: number,
  ): Promise<{ res: AdListLocalbitcoins[]; next: boolean }> {
    let next = false;
    const res = await this.httpService
      .get(`${urlLocalBitcoins}/${trade}-bitcoins-online/.json?page=${page}`)
      .pipe(
        map(response => {
          const query: ResReqLocalbitcoins = response.data;
          query.data.ad_count === 50 ? (next = true) : (next = false);
          return query.data.ad_list
            .filter(
              element =>
                element.data.countrycode !== 'VE'
            )
            .map(ad => {
              ad.data.trade_type = trade;
              return AdListLocalbitcoins.adListJson(ad, Provider.localbitcoins);
            });
        }),
      )
      .toPromise();
    return { res, next };
  }

  async getLocalCoinsSwapAll(coin_currency: string) {
    const res = await this.httpService
      .get(
        `https://localcoinswap.com/api/v2/offers/search/?coin_currency=${coin_currency}&limit=1600`,
      )
      .pipe(
        map(response => {
          let query: ResRepLocalCoinSwap = response.data;
          return query.results
            .map(ad =>
              AdListLocalCoinsSwap.adListJson(ad, Provider.localcoinswap),
            );
        }),
      )
      .toPromise();
    logger.log(`Datos cargados desde LocalCoinSwap`);
    return res;
  }

  async getHodls() {
    const res = await this.httpService
      .get(
        `https://hodlhodl.com/api/v1/offers`,
      )
      .pipe(
        map(response => {
          let query: ResReqHodl = response.data;
          return query.offers
            .filter(
                element =>
                element.working_now === true,
            )
            .map(ad =>
                AdListHodl.adListJson(ad, Provider.hodl),
            );
        }),
      )
      .toPromise();
    logger.log(`Datos cargados desde HodlsHodls`);
    return res;
  }

  async getPaxful(trade: string, currency: string): Promise<Ad[]> {
    const axios = require('axios');
    const qs = require('qs');
    const data = qs.stringify({
        'offer_type': trade,
        'payment_method': 'bank-transfer',
        'currency_code': currency,
        'limit': '300'
      });
      const config = {
        method: 'post',
        url: 'https://paxful.com/api/offer/all',
        headers: {
          'Content-Type': 'text/plain',
          'Accept': 'application/json'
        },
        data : data
      };
      const res: Ad[] = await axios(config)
        .then((response: { data: ResReqPaxful; }) => {
          let query: ResReqPaxful = response.data;
          console.log(query.data.offers.length);
          return query.data.offers
            .filter(
                element =>
                element.active === true,
            )
            .map(ad =>
                AdListPaxful.adListJson(ad, Provider.paxful),
            );
        })
        .catch( (error: any) => {
            console.log(error);
        });
      logger.log(`Datos cargados desde Paxful - ${trade} - ${currency}`);
      return res;

  }

  async getPriceMarket(currency: string): Promise<number> {
      const cur = currency === 'VES' ? 'VED' : currency;
    const res = await this.httpService
      .get(
        `${urlLocalBitcoins}/api/equation/max(bitstampusd_avg, bitfinexusd_avg)*USD_in_${cur}`,
      )
      .toPromise();
    if (res.data === '') {
      return 0;
    }
    const price: number = res.data.data;
    return price;
  }
}
