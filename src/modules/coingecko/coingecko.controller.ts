import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Query, Body, Param } from '@nestjs/common';
import { CoingeckoService } from './coingecko.service';
import { GetParamsCoinsDto, ResRepCoingeckoMarkets } from './dto';

@ApiTags('COINGECKO')
@Controller('coingecko')
export class CoingeckoController {
  constructor(private readonly coingeckoService: CoingeckoService) {}

  @Get()
  async getBitcoinsOnlineByCountry(
    @Query('vs_currency') vs_currency: string,
    @Query('order') order: string,
    @Query('per_page') per_page: string,
    @Query('page') page: string,
  ): Promise<ResRepCoingeckoMarkets[]> {
    const dto: GetParamsCoinsDto = {
      vs_currency,
      order,
      per_page,
      page,
    };
    const coinMarket = await this.coingeckoService.getCoingecko(dto);

    return coinMarket;
  }
}
