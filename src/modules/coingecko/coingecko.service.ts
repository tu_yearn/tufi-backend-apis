import { Injectable, HttpService } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { GetParamsCoinsDto, ResRepCoingeckoMarkets } from './dto';

@Injectable()
export class CoingeckoService {
  constructor(private httpService: HttpService) {}

  async getCoingecko(dto: GetParamsCoinsDto): Promise<ResRepCoingeckoMarkets[]> {
      try {
        const res = await this.httpService
      .get(
        `https://api.coingecko.com/api/v3/coins/markets?vs_currency=${dto.vs_currency}&order=${dto.order}&per_page=${dto.per_page}&page=${dto.page}&sparkline=true&price_change_percentage=1h,24h,7d`,
      )
      .pipe(
        map(response => {
          let query: ResRepCoingeckoMarkets[] = response.data;
          return query;
        }),
      )
      .toPromise();
    return res;
      } catch (error) {
          console.log(error.response)
          return error.response.data
      }

  }
}
