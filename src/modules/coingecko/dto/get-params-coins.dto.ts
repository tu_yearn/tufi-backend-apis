export class GetParamsCoinsDto {
    vs_currency:string;
    order: string;
    per_page: string;
    page: string;
}
