import { HttpModule, Module } from '@nestjs/common';
import { CoingeckoService } from './coingecko.service';
import { CoingeckoController } from './coingecko.controller';

@Module({
  providers: [CoingeckoService],
  controllers: [CoingeckoController],
  imports: [HttpModule]
})
export class CoingeckoModule {}
