import { adHodl } from '../modules/ads/dto';

export class AdListHodl {
  static adListJson(ad: adHodl, provider: string) {
    return new AdListHodl(
      ad.trader.login,
      this.convertNumeric(ad.trader.rating)*100,
      ad.trader.trades_count.toString(),
      new Date(),
      ad.working_now,
      this.convertDecimal(ad.price),
      'ALL_ONLINE',
      this.convertArrayToString(ad.payment_methods),
      this.convertNumeric(ad.min_amount),
      this.convertNumeric(ad.max_amount),
      ad.country,
      ad.payment_window_minutes,
      ad.description,
      new Date(),
      this.url(ad.id),
      ad.country_code,
      ad.side,
      ad.currency_code,
      ad.asset_code,
      provider,
    );
  }

  constructor(
    public name: string,
    public feedback_score: number,
    public trade_count: string,
    public last_online: Date,
    public visible: boolean,
    public temp_price: number,
    public online_provider: string,
    public bank_name: string,
    public min_amount: number,
    public max_amount: number,
    public location_string: string,
    public payment_window_minutes: number,
    public msg: string,
    public created_at: Date,
    public public_view: string,
    public countrycode: string,
    public trade_type: string,
    public fiat_currency: string,
    public coin_currency: string,
    public provider: string,
  ) {}

  static convertNumeric(str: any): number {
    let variable = parseInt(str);
    if (isNaN(variable)) {
      variable = 0;
    }
    return variable;
  }

  static convertDecimal(str: any): number {
    let variable = parseFloat(str);
    if (isNaN(variable)) {
      variable = 0;
    }
    return variable;
  }

  static convertDate(str: any): any {
    let variable = new Date(parseInt(str));
    if (str instanceof Date) {
      return variable;
    } else {
      return new Date();
    }
  }

  static convertArrayToString(str: any[]): string {
    if (str != null) {
        var names = str.map(function(item) {
            return item['name'];
          });
        return names.toString();
    } else {
        return ''
    }
  }

  static url(id: string): string {
      return `https://hodlhodl.com/offers/${id}/`
  }
}
