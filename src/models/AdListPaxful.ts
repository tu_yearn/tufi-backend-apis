import { AdPaxful } from '../modules/ads/dto';
import { currencyData } from 'src/config/data';

export class AdListPaxful {
  static adListJson(ad: AdPaxful, provider: string) {
    return new AdListPaxful(
      ad.offer_owner_username,
      ad.offer_owner_feedback_positive,
      (ad.offer_owner_feedback_positive + ad.offer_owner_feedback_negative)+'',
      new Date(),
      true,
      ad.fiat_price_per_btc,
      'ALL_ONLINE',
      ad.payment_method_label,
      ad.fiat_amount_range_min,
      ad.fiat_amount_range_max,
      this.findCountry(ad.currency_code),
      ad.payment_window,
      this.convertArrayToString(ad.tags),
      new Date(),
      ad.offer_link,
      this.findCodeCountry(ad.currency_code),
      ad.offer_type,
      ad.currency_code,
      'BTC',
      provider,
    );
  }

  constructor(
    public name: string,
    public feedback_score: number,
    public trade_count: string,
    public last_online: Date,
    public visible: boolean,
    public temp_price: number,
    public online_provider: string,
    public bank_name: string,
    public min_amount: number,
    public max_amount: number,
    public location_string: string,
    public payment_window_minutes: number,
    public msg: string,
    public created_at: Date,
    public public_view: string,
    public countrycode: string,
    public trade_type: string,
    public fiat_currency: string,
    public coin_currency: string,
    public provider: string,
  ) {}

  static convertNumeric(str: any): number {
    let variable = parseInt(str);
    if (isNaN(variable)) {
      variable = 0;
    }
    return variable;
  }

  static convertArrayToString(str: any[]): string {
    if (str != null) {
        var names = str.map(function(item) {
            return item['description'];
          });
        return names.toString();
    } else {
        return ''
    }
  }

  static findCountry(cur: string): string {
    const cd = currencyData.find(c => c.currency === cur);
    return cd.name
  }

  static findCodeCountry(cur: string): string {
    const cd = currencyData.find(c => c.currency === cur);
    return cd.code
  }
}
