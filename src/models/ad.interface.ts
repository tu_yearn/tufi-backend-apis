export interface Ad {
  name: string;
  feedback_score: number;
  trade_count: string;
  last_online: Date;
  visible: boolean;
  temp_price: number;
  online_provider: string;
  bank_name: string;
  min_amount: number;
  max_amount: number;
  location_string: string;
  payment_window_minutes: number;
  msg: string;
  created_at: Date;
  public_view: string;
  countrycode: string;
  trade_type: string;
  fiat_currency: string;
  coin_currency: string;
  provider: string;
}
