import { ResAdList } from '../modules/localbitcoins/dto';

export class AdListLocalbitcoins {
  static adListJson(ad: ResAdList, provider: string) {
    return new AdListLocalbitcoins(
      ad.data.profile.name,
      this.convertNumeric(ad.data.profile.feedback_score),
      ad.data.profile.trade_count,
      ad.data.profile.last_online,
      ad.data.visible,
      this.convertNumeric(ad.data.temp_price),
      ad.data.online_provider,
      ad.data.bank_name,
      this.convertNumeric(ad.data.min_amount),
      this.convertNumeric(ad.data.max_amount),
      ad.data.location_string,
      this.convertNumeric(ad.data.payment_window_minutes),
      ad.data.msg,
      ad.data.created_at,
      ad.actions.public_view,
      ad.data.countrycode,
      ad.data.trade_type,
      ad.data.currency === 'VED' ? 'VES' : ad.data.currency,
      "BTC",
      provider,
    );
  }

  constructor(
    public name: string,
    public feedback_score: number,
    public trade_count: string,
    public last_online: Date,
    public visible: boolean,
    public temp_price: number,
    public online_provider: string,
    public bank_name: string,
    public min_amount: number,
    public max_amount: number,
    public location_string: string,
    public payment_window_minutes: number,
    public msg: string,
    public created_at: Date,
    public public_view: string,
    public countrycode: string,
    public trade_type: string,
    public fiat_currency: string,
    public coin_currency: string,
    public provider: string,
  ) {}

  static convertNumeric(str: any): number {
    let variable = parseInt(str);
    if (isNaN(variable)) {
      variable = 0;
    }

    return variable;
  }
}
