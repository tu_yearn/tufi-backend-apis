import { adLocalCoinSwap } from '../modules/localcoinswap/dto';

export class AdListLocalCoinsSwap {
  static adListJson(ad: adLocalCoinSwap, provider: string) {
    return new AdListLocalCoinsSwap(
      ad.created_by.username,
      this.convertNumeric(ad.minimum_feedback),
      ad.created_by.completed_trades.toString(),
      this.convertDate(ad.created_by.last_seen),
      true,
      this.convertNumeric(ad.price_formula_value),
      ad.payment_method.name,
      '',
      this.convertNumeric(ad.min_trade_size),
      this.convertNumeric(ad.max_trade_size),
      ad.location_name,
      this.convertNumeric(ad.automatic_cancel_time),
      '',
      new Date(),
      this.url(ad.slug, ad.uuid),
      ad.country_code,
      ad.trading_type.opposite_name,
      ad.fiat_currency.symbol,
      ad.coin_currency.symbol,
      provider,
    );
  }

  constructor(
    public name: string,
    public feedback_score: number,
    public trade_count: string,
    public last_online: Date,
    public visible: boolean,
    public temp_price: number,
    public online_provider: string,
    public bank_name: string,
    public min_amount: number,
    public max_amount: number,
    public location_string: string,
    public payment_window_minutes: number,
    public msg: string,
    public created_at: Date,
    public public_view: string,
    public countrycode: string,
    public trade_type: string,
    public fiat_currency: string,
    public coin_currency: string,
    public provider: string,
  ) {}

  static convertNumeric(str: any): number {
    let variable = parseInt(str);
    if (isNaN(variable)) {
      variable = 0;
    }

    return variable;
  }

  static convertDate(str: any): any {
    let variable = new Date(parseInt(str));
    if (str instanceof Date) {
      return variable;
    } else {
      return new Date();
    }
  }

  static url(slug: string, uuid: string): string {
      return `https://localcoinswap.com/en/trade/${slug}/${uuid}/`
  }
}
