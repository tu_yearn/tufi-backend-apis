import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const initSwagger = (app: INestApplication) => {
    const swaggerConfing = new DocumentBuilder()
        .setTitle('TUFi API')
        .setDescription('API creada para TUFi')
        .build();
    const document = SwaggerModule.createDocument(app, swaggerConfing);
    SwaggerModule.setup('/docs', app, document);
}
