import { Logger } from "@nestjs/common";
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { Server, Socket } from "socket.io";
import {
  Ad,
  AdListLocalbitcoins,
  ListCoinsCoingeckoMarkets,
} from "./models";
import { CoinsService } from "./modules/coins/coins.service";
import { AdsService } from "./modules/ads/ads.service";

@WebSocketGateway(4501)
export class AppGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private readonly coinsService: CoinsService,
    private readonly adsService: AdsService
  ) {}

  @WebSocketServer() server: Server;
  private logger: Logger = new Logger("AppGateway");

  afterInit(server: Server) {
    this.logger.log("Initialized!");
    this.initLoadCoins();
    this.initLoadAdsLocalbitcoins();
    this.initLoadAdsLocalbitcoinsAll();
    this.initLoadAdsLocalCoinSwap();
    this.initLoadAdsHodls();
    this.initLoadAdsPaxful();
  }

  handleConnection(client: Socket): void {
    this.logger.log(`Client connected: ${client.id}`);
    const text = "hola";
    this.server.emit("msgToClient", text);
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage("msgToServer")
  handleMessage(client: Socket, text: string): void {
    this.logger.log(text);
    this.server.emit("msgToClient", "hola");
  }

  initLoadCoins() {
    this.logger.log("Actualizando tabla coins");
    setInterval(async () => {
      let dto: ListCoinsCoingeckoMarkets[] = [];
      for (let index = 1; index < 27; index++) {
        dto = dto.concat(await this.coinsService.getCoingecko(index));
      }
      await this.coinsService.create(dto);
      return {
        message: "Coin created",
      };
    }, 420000);
    return [];
  }

  initLoadAdsLocalbitcoins() {
    this.logger.log("Actualizando tabla Ads");
    setInterval(async () => {
      const dataCurrency = ["VES"];
      dataCurrency.forEach(async (currency) => {
        let result: Ad[] = [];
        const trade = ["buy", "sell"];
        trade.forEach(async (element) => {
          let i = 1;
          result = [];
          do {
            const data: {
              res: AdListLocalbitcoins[];
              next: boolean;
            } = await this.adsService.getLocalbitcoins(currency, element, i);
            result = result.concat(data.res);
            this.logger.log(
              `Processing ads ${currency} of ${element}, page Nro: ${i}`
            );
            i++;
            if (!data.next) i = 0;
          } while (i > 0);
          await this.adsService.create("localbitcoins", currency, result);
          this.logger.log(`Successful process ${currency} ${element}`);
        });
      });
    }, 300000);
    return [];
  }

  initLoadAdsLocalbitcoinsAll() {
    this.logger.log("Actualizando tabla Ads all");
    setInterval(async () => {
      let result: Ad[];
      const trade = ["buy", "sell"];

      trade.forEach(async (element) => {
        let i = 1;
        result = [];
        do {
          const data: {
            res: AdListLocalbitcoins[];
            next: boolean;
          } = await this.adsService.getLocalbitcoinsAll(element, i);
          result = result.concat(data.res);
          this.logger.log(`Processing ads of ${element}, page Nro: ${i}`);
          i++;
          if (!data.next) i = 0;
        } while (i > 0);
        await this.adsService.createAll("localbitcoins", result);
        this.logger.log(`Successful process ${element}`);
      });
    }, 1250000);
    return [];
  }

  initLoadAdsLocalCoinSwap() {
    this.logger.log("Actualizando tabla AdsLocalCoinSwap all");
    setInterval(async () => {
      const coin = ["BTC", "ETH"];
      coin.forEach(async (element) => {
        const res = await this.adsService.getLocalCoinsSwapAll(element);
        await this.adsService.createLocalCoinSwap(element,"localcoinswap",res);
        this.logger.log(`Successful process AdsLocalCoinSwap ${element}`);
      });
    }, 300000);
    return [];
  }

  initLoadAdsHodls() {
    this.logger.log("Actualizando tabla AdsHodls all");
    setInterval(async () => {
        const res = await this.adsService.getHodls();
        await this.adsService.createHodl("hodl",res);
        this.logger.log(`Successful process AdsHodl`);
    }, 300000);
    return [];
  }

  initLoadAdsPaxful() {
    this.logger.log("Actualizando tabla AdsPaxful all");
    setInterval(async () => {
        const trade = ["buy", "sell"];
        const currency = ["ARS","BRL","CAD","COP","CLP","DOP","MXN","PAB","PEN","UYU","GHS","NGN","CNY","GBP"];
        for (let i = 0; i < trade.length; i++) {
          for (let j = 0; j < currency.length; j++) {
            const res = await this.adsService.getPaxful(trade[i], currency[j]);
            await this.adsService.createPaxful(trade[i], currency[j],"paxful",res);
          };
        };
    }, 120000);
    return [];
  }
}
