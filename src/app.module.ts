import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UsersModule } from "./modules/users/users.module";
import { LocalbitcoinsModule } from "./modules/localbitcoins/localbitcoins.module";
import { TYPEORM_CONFIG } from "./config/constants";
import databaseConfig from "./config/database.config";
import { LocalCoinsSwapModule } from "./modules/localcoinswap/localcoinswap.module";
import { AppGateway } from "./app.gateway";
import { CoingeckoModule } from "./modules/coingecko/coingecko.module";
import { CoinsModule } from "./modules/coins/coins.module";
import { AdsModule } from "./modules/ads/ads.module";
import { PaymentMethodsModule } from "./modules/payment-methods/payment-methods.module";
import { ExchangesModule } from './modules/exchanges/exchanges.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) =>
        config.get<TypeOrmModuleOptions>(TYPEORM_CONFIG),
    }),
    UsersModule,
    LocalbitcoinsModule,
    LocalCoinsSwapModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [databaseConfig],
      envFilePath: ".env",
    }),
    CoingeckoModule,
    CoinsModule,
    AdsModule,
    PaymentMethodsModule,
    ExchangesModule,
  ],
  controllers: [AppController],
  providers: [AppService, AppGateway],
})
export class AppModule {}
